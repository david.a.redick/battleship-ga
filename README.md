One Day Hack

A simple genetic algorithm toy project for the battleship game.

This is for the fast variant of the US game:
* 8x8 grid
* Carrier - 5 size
* Battleship - 4 size
* Destroyer - 3 size
* Submarine - 3 size
* PT boat - 2 size

The front end `battleship-ga-board` - will produce a board arrangement of ships.

The front end `battleship-ga-shots` - will produce a list of shots to take.

The front end `battleship-ga-play` - will play a game by itself (run through a life cycle)

The genetic information is stored in a sqlite3 database.
This was done to show off the basics of python3 and sqlite3, it could easily be written into a text files.

--

Play time is up.  Shooter is interesting but plateaus quickly and not much variance.  Board layout is horribly broken.  No interactive playing.
