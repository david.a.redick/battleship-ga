#!/usr/bin/env python3

# https://docs.python.org/3.5/library/sqlite3.html
import sqlite3

import BoardLife
import ShooterLife

class Database:
	"""A Data Access Object style for the sqlite3 database.  Complete overkill."""

	def __init__(self):
		self.path_to_database = None
		self.connection = None

	def connect(self):
		# NOTE: This will create the db if doesn't exist.
		self.connection = sqlite3.connect(self.path_to_database)

	def close(self):
		self.connection.commit()
		self.connection.close()

	def _prime(self):
		"""Create the tables and things."""
		pass

	def save_a_BoardLife(self, boardLife):
		if None == boardLife.my_score:
			raise Exception('Unscored BoardLife %s' % boardLife.id)
		# insert rows
		cursor = self.connection.cursor()
		cursor.execute('INSERT INTO boardLifeScores VALUES (?, ?);', [boardLife.id, boardLife.my_score])
		cursor.execute('INSERT INTO boardLifeGenes VALUES (?, ?);', [boardLife.id, str(boardLife.genes)])

	def load_BoardLife(self, boardLifeId, score):
		cursor = self.connection.cursor()
		cursor.execute('SELECT genes FROM boardLifeGenes WHERE boardLifeId = ?;', [boardLifeId])
		row = cursor.fetchone()
		if None == row:
			raise Exception('No genes for boardLifeId %s' % boardLifeId)
			
		life = BoardLife.BoardLife(myId=boardLifeId, myGenes=eval(row[0]), myScore=score)
		return life

	def load_best_pair_BoardLife(self):
		cursor = self.connection.cursor()
		cursor.execute('SELECT * FROM boardLifeScores ORDER BY score DESC LIMIT 2;')
		rows = cursor.fetchmany(size=2)
		a = None
		b = None
		if 0 == len(rows):
			# Looks like an empty database.
			pass
		elif 1 == len(rows):
			a = self.load_BoardLife(rows[0][0], rows[0][1])
		elif 2 == len(rows):
			a = self.load_BoardLife(rows[0][0], rows[0][1])
			b = self.load_BoardLife(rows[1][0], rows[1][1])
		else:
			raise Exception('Should have only gotten at most 2, %d' % len(rows))

		return (a, b)

	def save_a_ShooterLife(self, shooterLife):
		if None == shooterLife.my_score:
			raise Exception('Unscored shooterLife %s' % shooterLife.id)
		# insert rows
		cursor = self.connection.cursor()
		cursor.execute('INSERT INTO shooterLifeScores VALUES (?, ?);', [shooterLife.id, shooterLife.my_score])
		cursor.execute('INSERT INTO shooterLifeGenes VALUES (?, ?);', [shooterLife.id, str(shooterLife.genes)])

	def load_ShooterLife(self, shooterLifeId, score):
		cursor = self.connection.cursor()
		cursor.execute('SELECT genes FROM shooterLifeGenes WHERE shooterLifeId = ?;', [shooterLifeId])
		row = cursor.fetchone()
		if None == row:
			raise Exception('No genes for shooterLifeId %s' % shooterLifeId)
			
		life = ShooterLife.ShooterLife(myId=shooterLifeId, myGenes=eval(row[0]), myScore=score)
		return life

	def load_best_pair_ShooterLife(self):
		cursor = self.connection.cursor()
		cursor.execute('SELECT * FROM shooterLifeScores ORDER BY score DESC LIMIT 2;')
		rows = cursor.fetchmany(size=2)
		a = None
		b = None
		if 0 == len(rows):
			# Looks like an empty database.
			pass
		elif 1 == len(rows):
			a = self.load_ShooterLife(rows[0][0], rows[0][1])
		elif 2 == len(rows):
			a = self.load_ShooterLife(rows[0][0], rows[0][1])
			b = self.load_ShooterLife(rows[1][0], rows[1][1])
		else:
			raise Exception('Should have only gotten at most 2, %d' % len(rows))

		return (a, b)
