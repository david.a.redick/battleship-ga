#!/usr/bin/env python3

import Ship

class Board:
	def __init__(self, size=8):
		self.width = size
		self.height = size

		# Map of ship name to tuple of (ship, orientation, row, col)
		self.ships = {}

		# row major size by size board
		# each cell is a list [ship, is_fire]
		# Where the ship is the pointer to the ship that occups the space (or None if it blank)
		# And is_fired is a boolean that determines if the enemy has dropped a bomb on that space.
		self.board = list(map(lambda x: list(map(lambda x: [None, False], range(size))), range(size)))

	def is_all_shunk(self):
		count = 0
		total = 0
		for ship_name in self.ships:
			ship = self.ships[ship_name][0]
			total += 1
			if ship.is_shunk():
				count += 1
		return count == total

	def can_ship_fit(self, ship, orientation, row, col):
		shipSize = ship.size-1 #-1 for the 0 based index.
		# Are we off the board?
		if 'vert' == orientation:
			last = shipSize + row
			if last >= self.width:
				return False
		elif 'horiz' == orientation:
			last = shipSize + col 
			if last >= self.height:
				return False
		else:
			raise Exception('Bad orientation %s' % orientation)

		# Is there another ship already in the way?
		if 'vert' == orientation:
			for i in range(ship.size):
				if None != self.board[row+i][col][0]:
					return False
		else:
			for i in range(ship.size):
				if None != self.board[row][col+i][0]:
					return False
		
		return True


	def add_ship(self, ship, orientation, row, col):
		"""Returns False if there isn't room for the ship."""
		if self.can_ship_fit(ship, orientation, row, col):
			self.ships[ship.name] = (ship, orientation, row, col)
			if 'vert' == orientation:
				for i in range(ship.size):
					self.board[row+i][col][0] = ship
			else:
				for i in range(ship.size):
					self.board[row][col+i][0] = ship
			return True
		else:
			return False

	def do_shot(self, row, col):
		"""Returns tuple (is_hit, shunk_ship) if the ship wasn't shunk then the second value will be None. Returns None if already bombed this spot."""
		cell = self.board[row][col]
		if True == cell[1]:
			# Duplicate shot.
			return None

		cell[1] = True
		result = None
		ship = cell[0]
		if None == ship:
			# A complete miss...
			result = (False, None)
		else:
			# Ship's be hit!
			if ship.do_hit():
				# Shunk!
				result = (True, ship)
			else:
				result = (True, None)
		return result

	def __str__(self):
		"""Pretty print the board. Capital letters denote a unhit part of the ship.  Lower case are hits. + are misses and . a unshot blank cells."""
		# spaces are for padding the row number and the pipe.
		s = '    '
		for col in range(self.width):
			s += '%d ' % col
		s += '\n  +'
		for col in range(self.width):
			s += '--'
		s += '\n'
		for row in range(self.height):
			s += '%d | ' % row
			for col in range(self.width):
				cell = self.board[row][col]
				if None == cell[0]:
					# Just ocean
					if True == cell[1]:
						s += '+'
					else:
						s += '.'
				else:
					# Thar be a ship...
					letter = cell[0].name[0]
					if True == cell[1]:
						# A hit.
						s += letter.lower()
					else:
						s += letter.upper()
				s += ' '
			s += '\n'
		return s

	def simple_setup(self):
		ships = Ship.make_ships()
		i = 0
		for ship in ships:
			if not self.add_ship(ship, 'horiz', i, 0):
				raise Exception('Could not add ship %s.' % ship.name)
			i += 1
		return self


if __name__ == '__main__':
	tooBig = Ship.Ship('TooBig', 100)
	verySmall = Ship.Ship('VerySmall', 1)
	sub = Ship.Ship('Submarine', 3)
	pt = Ship.Ship('PT boat', 2)
	b = Board()
	print(b.can_ship_fit(tooBig, 'vert', 0, 0))
	print(b.can_ship_fit(tooBig, 'horiz', 0, 0))
	print(b.can_ship_fit(verySmall, 'vert', 0, 0))
	print(b.can_ship_fit(verySmall, 'horiz', 0, 0))
	print(b.can_ship_fit(sub, 'vert', 0, 0))
	print(b.can_ship_fit(sub, 'horiz', 0, 6))

	print(b.add_ship(sub, 'horiz', 0, 0))
	print(b.add_ship(pt, 'vert', 5, 5))
	print(b.can_ship_fit(verySmall, 'horiz', 0, 0))
	print(b.can_ship_fit(verySmall, 'vert', 5, 5))

	print(b)
	print(b.do_shot(0,0))
	print(b)
	print(b.do_shot(0,0))
	print(b.do_shot(0,1))
	print(b.do_shot(0,2))
	print(b.do_shot(0,3))
	print(b)

	print('simple setup')
	another = Board()
	another.simple_setup()
	print(another)
	print(another.is_all_shunk())
