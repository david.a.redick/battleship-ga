#!/usr/bin/env python3

# https://docs.python.org/3.5/library/random.html
import random

# https://docs.python.org/3.5/library/uuid.html
import uuid

import Board
import Ship

number_of_ships = 5
number_of_genes_per_ship = 3
dna_size = number_of_ships * number_of_genes_per_ship

class BoardLife:
	"""A life form that make a ship layout on a board."""

	def __init__(self, myId=None, myGenes=None, myScore=None):
		if None == myId:
			self.id = str(uuid.uuid4()) # I don't care about keeping the UUID type.
		else:
			self.id = myId

		# Genes are small ints that provide the params to Board.add_ship:
		# orientation
		# row
		# col
		if 'random' == myGenes:
			self.genes = list(map(lambda x: random.getrandbits(4), range(dna_size)))
		else:
			self.genes = myGenes

		self.my_score = myScore

	def __lt__(self, other):
		return self.my_score < other.my_score

	def score(self):
		# A point if the orientation is 0 (vert) or 1 (horiz).
		# A point if the row is legal.
		# A point if the col is legal.
		# If all three are good then +10
		# +100 points if the ship can be added.
		# -1000 points if not all the ships were added.

		def is_legal(i, lower, upper):
			if i >= lower and i < upper:
				return True
			return False

		score = 0

		# Try to fill in the Nones.
		ships = [
			[Ship.Ship('Carrier', 5),    None, None, None], 
			[Ship.Ship('Battleship', 4), None, None, None], 
			[Ship.Ship('Destroyer', 3),  None, None, None], 
			[Ship.Ship('Submarine', 3),  None, None, None], 
			[Ship.Ship('PT boat', 2),    None, None, None]
		]

		gene_offset = 0
		good_ship_entries = 0
		b = Board.Board()
		for i in range(number_of_ships):
			row  = self.genes[gene_offset + 1]
			orientation = self.genes[gene_offset]
			col = self.genes[gene_offset + 2]
			gene_offset += 3

			all_three = 0
			if is_legal(orientation, 0, 2):
				score += 1
				all_three += 1
			if is_legal(row, 0, 8):
				score += 1
				all_three += 1
			if is_legal(col, 0, 8):
				score += 1
				all_three += 1

			if 3 == all_three:
				score += 10
				if 0 == orientation:
					orientation = 'vert'
				else:
					orientation = 'horiz'
				ships[i][1] = orientation
				ships[i][2] = row
				ships[i][3] = col

				is_added = b.add_ship(ships[i][0], ships[i][1], ships[i][2], ships[i][3])
				if is_added:
					score += 100
					good_ship_entries += 1
				#print((orientation, row, col))

		if good_ship_entries != number_of_ships:
			score -= 1000
		self.my_score = score
		return self.my_score

	def _crossBreed(self, a, b):
		"""Cross breed."""
		self.genes = list(map(lambda x: None, range(dna_size)))
		flipper=False
		for i in range(dna_size):
			if True == flipper:
				self.genes[i] = a.genes[i]
			else:
				self.genes[i] = b.genes[i]
			flipper = not flipper
		return self

	def _mutateBreed(self, x):
		"""Aggressively mutate."""
		self.genes = list(map(lambda x: None, range(dna_size)))
		flipper=False
		for i in range(dna_size):
			if True == flipper:
				self.genes[i] = x.genes[i]
			else:
				self.genes[i] = random.getrandbits(4)
			flipper = not flipper
		return self
		
	def breed(self, anotherBoardLife):
		"""Assumes both lifes have been scored. Returns list of two cross breeds and a mutant."""
		children = [None, None, None]

		children[0] = BoardLife()._crossBreed(self, anotherBoardLife)
		children[1] = BoardLife()._crossBreed(anotherBoardLife, self)

		if self.my_score >= anotherBoardLife.my_score:
			# mutate on self.
			children[2] = BoardLife()._mutateBreed(self)
		else:
			# mutate on another.
			children[2] = BoardLife()._mutateBreed(anotherBoardLife)

		return children

	def __str__(self):
		return self.id + '[' + str(self.my_score) + ']=' + str(self.genes)


if __name__ == '__main__':
	print('a')
	a = BoardLife(myGenes='random')
	a.score()
	print(a)

	print('b')
	b = BoardLife(myGenes='random')
	b.score()
	print(b)

	print('children')
	children = a.breed(b)
	for child in children:
		child.score()
		print(child)

