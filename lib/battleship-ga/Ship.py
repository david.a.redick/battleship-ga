class Ship:
	def __init__(self, name, size, hits=0):
		self.name = name
		self.size = size
		self.hits = hits

	def is_shunk(self):
		if self.hits >= self.size:
			# Prevent overflow
			self.hits = self.size
			return True
		return False

	def do_hit(self):
		"""Returns True if the ship was shunk."""
		self.hits += 1
		return self.is_shunk()

def make_ships():
	"""Returns a new list of all the ships we use in our game."""

	return [
		Ship('Carrier', 5),
		Ship('Battleship', 4),
		Ship('Destroyer', 3),
		Ship('Submarine', 3),
		Ship('PT boat', 2)
	]
