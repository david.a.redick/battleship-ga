#!/usr/bin/env python3

# https://docs.python.org/3.5/library/random.html
import random

# https://docs.python.org/3.5/library/uuid.html
import uuid

import Board

# The *2 is for a row and col
# The 8*5 is for covering at most a little over half the board.
dna_size = (8 * 5) * 2

class ShooterLife:
	"""A life form that drops bombs on a board."""

	def __init__(self, myId=None, myGenes=None, myScore=None):
		if None == myId:
			self.id = str(uuid.uuid4()) # I don't care about keeping the UUID type.
		else:
			self.id = myId

		# Genes are small ints that provide the params to Board.do_shot:
		# row
		# col
		if 'random' == myGenes:
			self.genes = list(map(lambda x: random.getrandbits(3), range(dna_size)))
		else:
			self.genes = myGenes

		self.my_score = myScore

	def __lt__(self, other):
		return self.my_score < other.my_score

	def __str__(self):
		return self.id + '[' + str(self.my_score) + ']=' + str(self.genes)

	def score(self, board):
		"""Dont share boards.  Board will be updated."""
		# A point if valid coordinate.
		# -10 we repeat bomb a location.
		# +10 if we hit something
		# +50 if shink a ship

		def is_legal(i, lower, upper):
			if i >= lower and i < upper:
				return True
			return False

		score = 0

		offset = 0
		while offset < dna_size:
			row = self.genes[offset]
			col = self.genes[offset+1]
			offset += 2

			both = 0
			if is_legal(row, 0, 8):
				score += 1
				both += 1
			if is_legal(col, 0, 8):
				score += 1
				both += 1

			if 2 == both:
				#print('a legal shot %d %d' % (row, col))
				result = board.do_shot(row, col)
				if None == result:
					#print('Duplicate')
					score -= 10
				else:
					if result[0]:
						# A hit.
						score += 10
					if result[1]:
						# SHIP SHANK!
						score += 50

		self.my_score = score
		return self.my_score

	def _crossBreed(self, a, b):
		"""Cross breed."""
		self.genes = list(map(lambda x: None, range(dna_size)))
		flipper=False
		for i in range(dna_size):
			if True == flipper:
				self.genes[i] = a.genes[i]
			else:
				self.genes[i] = b.genes[i]
			flipper = not flipper
		return self

	def _mutateBreed(self, x):
		"""Aggressively mutate."""
		self.genes = list(map(lambda x: None, range(dna_size)))
		flipper=False
		for i in range(dna_size):
			if True == flipper:
				self.genes[i] = x.genes[i]
			else:
				self.genes[i] = random.getrandbits(3)
			flipper = not flipper
		return self

	def breed(self, anotherShooterLife):
		"""Assumes both lifes have been scored. Returns list of two cross breeds and two mutants."""
		children = [None, None, None, None]

		children[0] = ShooterLife()._crossBreed(self, anotherShooterLife)
		children[1] = ShooterLife()._crossBreed(anotherShooterLife, self)
		children[2] = ShooterLife()._mutateBreed(self)
		children[3] = ShooterLife()._mutateBreed(anotherShooterLife)

		return children


if __name__ == '__main__':
	print('a')
	a = ShooterLife(myGenes='random')
	a.score(Board.Board().simple_setup())
	print(a)

	print('b')
	b = ShooterLife(myGenes='random')
	b.score(Board.Board().simple_setup())
	print(b)

	print('children')
	children = a.breed(b)
	for child in children:
		child.score(Board.Board().simple_setup())
		print(child)
