
DATABASE_NAME=db.sqlite3
DATABASE=./var/lib/battleship-ga/$(DATABASE_NAME)

.PHONY: prime
prime:
	rm -f $(DATABASE)
	# one to one
	sqlite3 $(DATABASE) 'CREATE TABLE boardLifeScores(boardLifeId UNIQUE, score INTEGER);'
	# one to one
	sqlite3 $(DATABASE) 'CREATE TABLE boardLifeGenes(boardLifeId UNIQUE, genes);'
	# one to one
	sqlite3 $(DATABASE) 'CREATE TABLE shooterLifeScores(shooterLifeId UNIQUE, score INTEGER);'
	# one to one
	sqlite3 $(DATABASE) 'CREATE TABLE shooterLifeGenes(shooterLifeId UNIQUE, genes);'

.PHONY: sql
sql:
	sqlite3 $(DATABASE)

.PHONY: inspect
inspect:
	sqlite3 $(DATABASE) 'SELECT * FROM boardLifeScores;'
	sqlite3 $(DATABASE) 'SELECT * FROM boardLifeGenes;'
	sqlite3 $(DATABASE) 'SELECT * FROM shooterLifeScores;'
	sqlite3 $(DATABASE) 'SELECT * FROM shooterLifeGenes;'

